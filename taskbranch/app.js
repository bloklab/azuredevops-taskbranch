﻿
VSS.init({
    explicitNotifyLoaded: true,
    usePlatformScripts: true
});



// Retrieve the basic information we need to display the element

VSS.require(["TFS/WorkItemTracking/Services", "VSS/Authentication/Services", "TFS/VersionControl/GitRestClient"], function (_WorkItemServices, _authService, _git) {

    // Get the WI service
    function getWorkItemFormService() {
        return _WorkItemServices.WorkItemFormService.getService();
    }

    var provider = function () {
        return {
            onLoaded: function (args) {

                var context = VSS.getWebContext();
                var config = VSS.getConfiguration();

                var organisation = context.collection.name;
                var project = context.project.name;
                var repository = config.witInputs.repositoryId;

                var repo_url = "https://dev.azure.com/" + organisation + "/" + project + "/_apis/git/repositories/" + repository + "/refs?api-version=4.1";

                //VSS.getAccessToken().then(function (sessionToken) {

                //    console.log(sessionToken);

                //    var authorizationHeaderValue = _authService.authTokenManager.getAuthorizationHeader(sessionToken);
                //    $.ajax({
                //        url: repo_url,
                //        method: "GET",
                //        success: function (data, textStatus, jqueryXHR) {
                //            success(data);
                //        },
                //        error: function (jqXHR, textStatus, errorThrown) {

                //        },
                //        beforeSend: function (jqXHR) {
                //            jqXHR.setRequestHeader("Authorization", authorizationHeaderValue);
                //        }
                //    });
                //});

                var gitClient = _git.getClient();

                gitClient.getBranches(repository, project).then(console.log);

                gitClient.getRepositories().then(console.log);

                gitClient.updateRefs([{
                    name: "refs/heads/issues/test-new",
                    newObjectId: "0c4e2bc0cf07688898985f75d72a5b56fe66ba82",
                    oldObjectId: "0000000000000000000000000000000000000000"
               }], repository, project).then(console.log);


                getWorkItemFormService().then(function (service) {
                    // Get the current values for a few of the common fields
                    service.getFieldValues(["System.Id", "System.Title", "System.State", "System.CreatedDate"]).then(
                        function (value) {
                            console.log(JSON.stringify(value));
                        });

                    service.getWorkItemRelations().then(console.log);

                    //ArtifactLink
                    if (true) {
                        service.addWorkItemRelations([{
                            "attributes": {"name": "Branch"},
                            "rel": "ArtifactLink",
                            "url": "vstfs:///Git/Ref/297b3b70-d729-4879-b5ae-cdf622b0282e%2F9df2ecd4-409c-4c1f-bfbc-be2d5e0b7601%2FGBissues%2F1337-test"
                        }]).then(service.save);
                    }
                });

                document.getElementById("name").innerText = VSS.getWebContext().user.name;
            },
            onUnloaded: function (args) { debugger; },
            onFieldChanged: function (args) { debugger; }
        };
    };

    VSS.register(VSS.getContribution().id, provider);
    VSS.notifyLoadSucceeded();

    console.log(VSS.getContribution().id);
    console.log(VSS.getWebContext());
    console.log(VSS.getConfiguration());

    //https://dev.azure.com/fabrikam/_apis/git/repositories/278d5cd2-584d-4b63-824a-2ba458937249/refs?api-version=4.1
});


        //var provider = () => {
        //    var ensureControl = () => {
        //        if (!control) {
        //            var inputs: IDictionaryStringTo<string> = VSS.getConfiguration().witInputs;
        //            var controlType: string = inputs["InputMode"];
        //            control = new MultiValueCombo();

        //            control.initialize();
        //        }

        //        control.invalidate();
        //    };

        //    return {
        //        onLoaded: (args: WitExtensionContracts.IWorkItemLoadedArgs) => {
        //            ensureControl();
        //        },
        //        onUnloaded: (args: WitExtensionContracts.IWorkItemChangedArgs) => {
        //            if (control) {
        //                control.clear();
        //            }
        //        },
        //        onFieldChanged: function(args) {
        //            if (control && args.changedFields[control.fieldName] !== undefined && args.changedFields[control.fieldName] !== null) {
        //                control.invalidate();
        //            }
        //        }
        //    }
        //};

        //